#!/bin/bash
# Stress test for Javanaise
# Usage: ./burstScript.sh <nb_threads> <nb_objects>
echo "## Burst Stress test for Javanaise"

# Start a Coordinator
echo "# Start Coordinator"
java -cp "$(pwd)"/bin jvn.JvnCoordImpl &
COORD_PID=$!
# Sleep for 3s for the Coordinator to get ready
sleep 3

# Start N processes of BurstTest at the same time
echo "# Coordinator should be ready, start $1 processes with $2 objects"
PIDS=()
for i in $(seq 1 "$1")
do
  java -cp "$(pwd)"/bin burst.BurstTest "$2" & PIDS+=("$!")
done

# Wait until all tests are done
for p in "${PIDS[@]}"
do
  wait "$p"
done

# Kill Coordinator
echo "# Tests are done, killing Coordinator"
kill $COORD_PID

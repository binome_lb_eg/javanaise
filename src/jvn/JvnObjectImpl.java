package jvn;

import java.io.Serializable;

public class JvnObjectImpl implements JvnObject {
	private Serializable obj;
	private int id;
	private LockState lock;

	public JvnObjectImpl(Serializable obj, int id) {
		this.obj = obj;
		this.id = id;
		this.lock = LockState.NOLOCK;
	}

	@Override
	public void jvnLockRead() throws JvnException {
		switch (this.lock) {
			case NOLOCK:
				this.obj = JvnServerImpl.jvnGetServer().jvnLockRead(this.id);
				this.lock = LockState.READ;
				break;

			case WRITE:
			case WRITECACHED:
				//this.obj = JvnServerImpl.jvnGetServer().jvnLockRead(this.id);
				this.lock = LockState.READWRITECACHED;
				break;

			case READCACHED:
				this.lock = LockState.READ;
				break;

			default:
				break;
		}
	}

	@Override
	public void jvnLockWrite() throws JvnException {
		switch (this.lock) {
			case NOLOCK:
			case READ:
			case READCACHED:
				this.obj = JvnServerImpl.jvnGetServer().jvnLockWrite(this.id);
				this.lock = LockState.WRITE;
				break;

			case WRITECACHED:
				this.lock = LockState.WRITE;
				break;

			default:
				break;
		}
	}

	@Override
	public synchronized void jvnUnLock() throws JvnException {
		switch (this.lock) {
			case READ:
				this.lock = LockState.READCACHED;
				break;

			case WRITE:
			case READWRITECACHED:
				this.lock = LockState.WRITECACHED;
				break;

			default:
				break;
		}
		this.notify();
	}

	@Override
	public int jvnGetObjectId() throws JvnException {
		return this.id;
	}

	@Override
	public Serializable jvnGetSharedObject() throws JvnException {
		return obj;
	}

	@Override
	public synchronized void jvnInvalidateReader() throws JvnException {
		try {
			switch (this.lock) {
				case READ:
					this.wait(); // Wait until the Object is unlocked, will be put in READCACHED on notify
					this.lock = LockState.NOLOCK;
					break;

				case READCACHED:
					this.lock = LockState.NOLOCK;
					break;

				default:
					break;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public synchronized Serializable jvnInvalidateWriter() throws JvnException {
		try {
			switch (this.lock) {
				case WRITE:
				case READWRITECACHED:
					this.wait(); // Wait until the Object is unlocked, will be put in WRITECACHED on notify
					this.lock = LockState.NOLOCK;
					break;

				case WRITECACHED:
					this.lock = LockState.NOLOCK;
					break;

				default:
					break;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return this.obj;
	}

	@Override
	public synchronized Serializable jvnInvalidateWriterForReader() throws JvnException {
		try {
			switch (this.lock) {
				case WRITE:
					this.wait(); // Wait until the Object is unlocked, will be put in WRITECACHED on notify
					this.lock = LockState.READCACHED;
					break;

				case WRITECACHED:
					this.lock = LockState.READCACHED;
					break;

				case READWRITECACHED:
					this.lock = LockState.READ;
					break;

				default:
					break;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return this.obj;
	}
}

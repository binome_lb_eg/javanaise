package jvn.proxy;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import jvn.JvnException;
import jvn.JvnObject;
import jvn.JvnServerImpl;
import jvn.annotations.JvnMethod;

public class JvnObjectProxy implements InvocationHandler {
	private static JvnServerImpl js = JvnServerImpl.jvnGetServer(); // Start JvnServer
	private JvnObject obj;

	private JvnObjectProxy(JvnObject o) {
		this.obj = o;
	}

	public static Object newInstance(Serializable obj, String s) {
		JvnObject jo;
		try {
			// Check if object already exists
			jo = js.jvnLookupObject(s);
			// If it doesn't, create it and register it
			if (jo == null) {
				jo = js.jvnCreateObject(obj);
				js.jvnRegisterObject(s, jo);
				jo.jvnUnLock();
			}

		} catch (JvnException e) {
			// Object most likely got created at the same time, so we just get it with a lookup
			try {
				jo = js.jvnLookupObject(s);
			} catch (JvnException e1) {
				// If it fails again, it's not good :(
				e1.printStackTrace();
				return null;
			}
		}

		// Return Proxy object
		return Proxy.newProxyInstance(
			obj.getClass().getClassLoader(),
			obj.getClass().getInterfaces(),
			new JvnObjectProxy(jo));
	}


	@Override
	public Object invoke(Object proxy, Method m, Object[] args) throws Throwable {
		if (m.isAnnotationPresent(JvnMethod.class)) {
			JvnMethod meth = m.getAnnotation(JvnMethod.class);
			switch(meth.value()) {
				// Lock read before calling
				case READ:
					this.obj.jvnLockRead();
					break;

				// Lock write before calling
				case READWRITE:
				case WRITE:
					this.obj.jvnLockWrite();
					break;
			}
		}
		// Invoke the called method on the shared object
		Object result = m.invoke(this.obj.jvnGetSharedObject(), args);

		// Unlock after use
		this.obj.jvnUnLock();

		return result;
	}
}

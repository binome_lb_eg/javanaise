package jvn;

/**
 * Class containing some constants used in both Coordinator and Server classes
 */
public class JvnConstants {
	public final static String JVN_SERVICENAME = "CoordService";
	public static final int RMI_PORT = 12345;
}

package jvn.annotations;
import java.lang.annotation.*;

/**
 * Annotation used to specify the Access Policy of a method of a JvnObject
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface JvnMethod {
	/**
	 * Type of access for a JvnMethod annotation
	 */
	public enum AccessPolicy {
		READ, WRITE, READWRITE
	}

	AccessPolicy value();
}

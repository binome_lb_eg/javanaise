package jvn;

public enum LockState {
	/** No lock */
	NOLOCK,
	/** Read lock cached */
	READCACHED,
	/** Write lock cached */
	WRITECACHED,
	/** Read lock taken */
	READ,
	/** Write lock taken */
	WRITE,
	/** Write lock cached & read lock taken */
	READWRITECACHED
}

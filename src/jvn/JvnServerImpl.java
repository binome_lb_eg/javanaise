/***
 * JAVANAISE Implementation
 * JvnServerImpl class
 * Implementation of a Jvn server
 * Contact:
 *
 * Authors:
 */

package jvn;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.io.*;

public class JvnServerImpl extends UnicastRemoteObject implements JvnLocalServer, JvnRemoteServer {
	private static final long serialVersionUID = 1L;
	// A JVN server is managed as a singleton
	private static JvnServerImpl js = null;
	private JvnRemoteCoord jvnCoord;
	private HashMap<Integer, JvnObject> objList;

	/**
	 * Default constructor
	 *
	 * @throws JvnException
	 **/
	private JvnServerImpl() throws Exception {
		super();
		this.objList = new HashMap<>();

		connectServer();
	}

	private void connectServer() {
		// Connect to RMI Registry
		Registry registry = null;
		try {
			registry = LocateRegistry.getRegistry("localhost", JvnConstants.RMI_PORT); // FIXME: not a local address here :(
			this.jvnCoord = (JvnRemoteCoord) registry.lookup(JvnConstants.JVN_SERVICENAME);
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Static method allowing an application to get a reference to a JVN server
	 * instance
	 *
	 * @throws JvnException
	 **/
	public static JvnServerImpl jvnGetServer() {
		if (js == null) {
			try {
				js = new JvnServerImpl();
			} catch (Exception e) {
				return null;
			}
		}
		return js;
	}

	/**
	 * Flush local cache and notify the coordinator to unlock
	 * @throws jvn.JvnException
	 */
	public void jvnFlush() throws jvn.JvnException {
		try {
			// Notify the coordinator so that he can remove references/locks we currently have
			this.jvnTerminate();
			// Clear our object list
			this.objList.clear();
		} catch (Exception e) {
			// reconnect to the server
			connectServer();
			e.printStackTrace();
		}
	}

	/**
	 * The JVN service is not used anymore
	 *
	 * @throws JvnException
	 **/
	public void jvnTerminate() throws jvn.JvnException {
		try {
			this.jvnCoord.jvnTerminate(jvnGetServer());
		} catch (RemoteException e) {
			// reconnect to the server
			connectServer();
			try {
				this.jvnCoord.jvnTerminate(jvnGetServer());
			} catch (RemoteException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * creation of a JVN object
	 *
	 * @param o : the JVN object state
	 * @throws JvnException
	 **/
	public JvnObject jvnCreateObject(Serializable o) throws jvn.JvnException {
		try {
			return new JvnObjectImpl(o, this.jvnCoord.jvnGetObjectId());
		} catch (RemoteException e) {
			// reconnect to the server
			connectServer();
			try {
				return new JvnObjectImpl(o, this.jvnCoord.jvnGetObjectId());
			} catch (RemoteException ex) {
				return null;
			}
		}
	}

	/**
	 * Associate a symbolic name with a JVN object
	 *
	 * @param jon : the JVN object name
	 * @param jo  : the JVN object
	 * @throws JvnException
	 **/
	public void jvnRegisterObject(String jon, JvnObject jo) throws jvn.JvnException {
		try {
			this.jvnCoord.jvnRegisterObject(jon, jo, jvnGetServer());
			jo.jvnLockWrite(); // Write Lock by default

			// Keep track of object and lock
			this.objList.put(jo.jvnGetObjectId(), jo);

		} catch (RemoteException e) {
			// reconnect to the server
			connectServer();
			try {
				this.jvnCoord.jvnRegisterObject(jon, jo, jvnGetServer());
			jo.jvnLockWrite(); // Write Lock by default

			// Keep track of object and lock
			this.objList.put(jo.jvnGetObjectId(), jo);
			} catch (RemoteException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Provide the reference of a JVN object beeing given its symbolic name
	 *
	 * @param jon : the JVN object name
	 * @return the JVN object
	 * @throws JvnException
	 **/
	public JvnObject jvnLookupObject(String jon) throws jvn.JvnException {
		JvnObject jo = null;
		try {
			jo = this.jvnCoord.jvnLookupObject(jon, jvnGetServer());

			if (jo != null) {
				// Keep track of object and lock
				this.objList.put(jo.jvnGetObjectId(), jo);
				return jo;
			}

		} catch (RemoteException e) {
			// reconnect to the server
			connectServer();
			try {
				jo = this.jvnCoord.jvnLookupObject(jon, jvnGetServer());

			if (jo != null) {
				// Keep track of object and lock
				this.objList.put(jo.jvnGetObjectId(), jo);
				return jo;
			}
			} catch (RemoteException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Get a Read lock on a JVN object
	 *
	 * @param joi : the JVN object identification
	 * @return the current JVN object state
	 * @throws JvnException
	 **/
	public Serializable jvnLockRead(int joi) throws JvnException {
		try {
			return jvnCoord.jvnLockRead(joi, this);
		} catch (RemoteException e) {
			// reconnect to the server
			connectServer();
			try {
				return jvnCoord.jvnLockRead(joi, this);
			} catch (RemoteException ex) {
				ex.printStackTrace();
				return null;
			}
		}
	}

	/**
	 * Get a Write lock on a JVN object
	 *
	 * @param joi : the JVN object identification
	 * @return the current JVN object state
	 * @throws JvnException
	 **/
	public Serializable jvnLockWrite(int joi) throws JvnException {
		try {
			return jvnCoord.jvnLockWrite(joi, this);
		} catch (RemoteException e) {
			// reconnect to the server
			connectServer();
			try {
				return jvnCoord.jvnLockWrite(joi, this);
			} catch (RemoteException ex) {
				ex.printStackTrace();
				return null;
			}
		}
	}

	/**
	 * Invalidate the Read lock of the JVN object identified by id called by the
	 * JvnCoord
	 *
	 * @param joi : the JVN object id
	 * @return void
	 * @throws java.rmi.RemoteException,JvnException
	 **/
	public void jvnInvalidateReader(int joi) throws java.rmi.RemoteException, jvn.JvnException {
		this.objList.get(joi).jvnInvalidateReader();
	};

	/**
	 * Invalidate the Write lock of the JVN object identified by id
	 *
	 * @param joi : the JVN object id
	 * @return the current JVN object state
	 * @throws java.rmi.RemoteException,JvnException
	 **/
	public Serializable jvnInvalidateWriter(int joi) throws java.rmi.RemoteException, jvn.JvnException {
		return this.objList.get(joi).jvnInvalidateWriter();
	};

	/**
	 * Reduce the Write lock of the JVN object identified by id
	 *
	 * @param joi : the JVN object id
	 * @return the current JVN object state
	 * @throws java.rmi.RemoteException,JvnException
	 **/
	public Serializable jvnInvalidateWriterForReader(int joi) throws java.rmi.RemoteException, jvn.JvnException {
		return this.objList.get(joi).jvnInvalidateWriterForReader();
	};

}

/***
 * JAVANAISE Implementation
 * JvnCoordImpl class
 * This class implements the Javanaise central coordinator
 * Contact:
 *
 * Authors:
 */

package jvn;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.Serializable;

public class JvnCoordImpl extends UnicastRemoteObject implements JvnRemoteCoord {
	private static final long serialVersionUID = 1L;

	/** Link between an object name and its ID */
	HashMap<String, Integer> idList;
	/** Link between an object ID and its reference */
	HashMap<Integer, JvnObject> objList;
	/** Link between an object ID and its LockState for each connected server */
	HashMap<Integer, HashMap<JvnRemoteServer, LockState>> lockList;

	/** Number of objects allocated */
	AtomicInteger nbObj;

	/**
	 * Default constructor
	 *
	 * @throws JvnException
	 **/
	protected JvnCoordImpl() throws Exception {
		// Bind Coordinator to the RMI register
		Registry reg = LocateRegistry.createRegistry(JvnConstants.RMI_PORT);
		reg.bind(JvnConstants.JVN_SERVICENAME, this);

		this.idList = new HashMap<>();
		this.objList = new HashMap<>();
		this.lockList = new HashMap<>();

		this.nbObj = new AtomicInteger(0);
	}

	/**
	 * Allocate a NEW JVN object id (usually allocated to a newly created JVN
	 * object)
	 *
	 * @throws java.rmi.RemoteException,JvnException
	 **/
	public int jvnGetObjectId() throws java.rmi.RemoteException, jvn.JvnException {
		return this.nbObj.getAndIncrement();
	}

	/**
	 * Associate a symbolic name with a JVN object
	 *
	 * @param jon : the JVN object name
	 * @param jo  : the JVN object
	 * @param js  : the remote reference of the JVNServer
	 * @throws java.rmi.RemoteException,JvnException
	 **/
	public synchronized void jvnRegisterObject(String jon, JvnObject jo, JvnRemoteServer js) throws java.rmi.RemoteException, jvn.JvnException {
		// Fail if the object name already exists
		if (this.idList.containsKey(jon))
			throw new JvnException("Object with name " + jon + " already exists");

		// Allocate and register object
		Integer id = jo.jvnGetObjectId();
		this.idList.put(jon, id);
		this.objList.put(id, jo);

		HashMap<JvnRemoteServer, LockState> lock = new HashMap<>();
		lock.put(js, LockState.WRITE); // Write lock by default
		this.lockList.put(id, lock);
	}

	/**
	 * Get the reference of a JVN object managed by a given JVN server
	 *
	 * @param jon : the JVN object name
	 * @param js  : the remote reference of the JVNServer
	 * @throws java.rmi.RemoteException,JvnException
	 **/
	public synchronized JvnObject jvnLookupObject(String jon, JvnRemoteServer js) throws java.rmi.RemoteException, jvn.JvnException {
		// Fail if the object name doesn't exist
		if (!this.idList.containsKey(jon))
			return null;

		Integer id = this.idList.get(jon);
		this.lockList.get(id).put(js, LockState.NOLOCK); // Server js now has the object, track lock state for it

		return this.objList.get(this.idList.get(jon));
	}

	/**
	 * Get a Read lock on a JVN object managed by a given JVN server
	 *
	 * @param joi : the JVN object identification
	 * @param js  : the remote reference of the server
	 * @return the current JVN object state
	 * @throws java.rmi.RemoteException, JvnException
	 **/
	public synchronized Serializable jvnLockRead(int joi, JvnRemoteServer js) throws java.rmi.RemoteException, JvnException {
		HashMap<JvnRemoteServer, LockState> objLockList = this.lockList.get(joi);
		Entry<JvnRemoteServer, LockState> requester = null;

		for (Entry<JvnRemoteServer, LockState> lock : objLockList.entrySet()) {
			// Get the Pair corresponding to the requester
			if (lock.getKey().equals(js)) {
				requester = lock;
			}

			switch (lock.getValue()) {
				// If a server has a write lock, invalidate it and put it in read lock instead
				case WRITE:
				case WRITECACHED:
					this.objList.put(joi, new JvnObjectImpl(lock.getKey().jvnInvalidateWriterForReader(joi), joi));
					objLockList.put(lock.getKey(), LockState.READCACHED);
					break;
				case READWRITECACHED:
					this.objList.put(joi, new JvnObjectImpl(lock.getKey().jvnInvalidateWriterForReader(joi), joi));
					objLockList.put(lock.getKey(), LockState.READ);
					break;

				default:
					break;
			}
		}

		// Set new lock state for the requester
		switch (requester.getValue()) {
			// If requester has a write lock, put it in write cached + read lock
			case WRITE:
			case WRITECACHED:
				objLockList.put(requester.getKey(), LockState.READWRITECACHED);
				break;

			// If requester doesn't have a lock, put it in read lock
			case NOLOCK:
			case READCACHED:
				objLockList.put(requester.getKey(), LockState.READ);

			default:
				break;
		}

		return this.objList.get(joi).jvnGetSharedObject();
	}

	/**
	 * Get a Write lock on a JVN object managed by a given JVN server
	 *
	 * @param joi : the JVN object identification
	 * @param js  : the remote reference of the server
	 * @return the current JVN object state
	 * @throws java.rmi.RemoteException, JvnException
	 **/
	public synchronized Serializable jvnLockWrite(int joi, JvnRemoteServer js) throws java.rmi.RemoteException, JvnException {
		HashMap<JvnRemoteServer, LockState> objLockList = this.lockList.get(joi);
		Entry<JvnRemoteServer, LockState> requester = null;

		for (Entry<JvnRemoteServer, LockState> lock : objLockList.entrySet()) {
			// Get the Pair corresponding to the requester
			if (lock.getKey().equals(js)) {
				requester = lock;
				continue;
			}

			switch (lock.getValue()) {
				// If a server has a write lock, invalidate it
				case WRITE:
				case WRITECACHED:
				case READWRITECACHED:
					this.objList.put(joi, new JvnObjectImpl(lock.getKey().jvnInvalidateWriter(joi), joi));
					objLockList.put(lock.getKey(), LockState.NOLOCK);
					break;

				// If a server has a read lock, invalidate it
				case READ:
				case READCACHED:
					lock.getKey().jvnInvalidateReader(joi);
					objLockList.put(lock.getKey(), LockState.NOLOCK);
					break;

				default:
					break;
			}
		}

		// Set new lock state for the requester
		switch (requester.getValue()) {
			// If requester has no lock or a read lock, put it in write lock
			case NOLOCK:
			case READ:
			case READCACHED:
			case WRITECACHED:
			case READWRITECACHED:
				objLockList.put(requester.getKey(), LockState.WRITE);
				break;

			default:
				break;
		}

		return this.objList.get(joi).jvnGetSharedObject();
	}

	/**
	 * A JVN server terminates
	 *
	 * @param js : the remote reference of the server
	 * @throws java.rmi.RemoteException, JvnException
	 **/
	public synchronized void jvnTerminate(JvnRemoteServer js) throws java.rmi.RemoteException, JvnException {
		// Check if js has a write lock on a JvnObject before removing it from the list
		// If it has one, free the lock and get the latest JvnObject value
		for (Entry<Integer, HashMap<JvnRemoteServer, LockState>> locks : lockList.entrySet()) {
			switch (locks.getValue().get(js)) {
				case WRITE:
				case WRITECACHED:
				case READWRITECACHED:
					this.objList.put(locks.getKey(), new JvnObjectImpl(js.jvnInvalidateWriter(locks.getKey()), locks.getKey()));
					break;

				default:
					break;
			}
			locks.getValue().remove(js);
		}
	}


	/** Start a simple Coordinator */
	public static void main(String[] args) {
		try {
			JvnCoordImpl jvnCoordImpl = new JvnCoordImpl();
			System.out.println("Coordinator Ready");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package jvn;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class JvnCoordPersistentImpl extends JvnCoordImpl {
	/** Path and name of the file to save into */
	private String filepath;

	private JvnCoordPersistentImpl(String filepath) throws Exception {
		super();
		this.filepath = filepath;
	}

	/**
	 * Try to load existing Coordinator data from disk, otherwise create a new one
	 * @param filepath Path of the file to try to read from
	 * @return A new JvnCoordPersistentImpl
	 * @throws Exception
	 */
	public static JvnCoordPersistentImpl tryLoad(String filepath) throws Exception {
		JvnCoordPersistentImpl jc;

		try (ObjectInputStream objectIn = new ObjectInputStream(new FileInputStream(filepath))) {
			// Load existing data from disk if it exists
			jc = (JvnCoordPersistentImpl) objectIn.readObject();
			System.out.println("Coordinator data loaded from file: " + filepath);
			// Register Coordinator in RMI Registry
			Registry reg = LocateRegistry.createRegistry(JvnConstants.RMI_PORT);
			reg.bind(JvnConstants.JVN_SERVICENAME, jc);

		} catch (Exception e) {
			// On error, create a new JvnCoord
			System.out.println("Couldn't find Coordinator data, creating a new one.");
			jc = new JvnCoordPersistentImpl(filepath);
		}

		return jc;
	}

	private void saveState() {
		try (ObjectOutputStream objectOut = new ObjectOutputStream(new FileOutputStream(this.filepath))) {
			objectOut.writeObject(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public synchronized Serializable jvnLockRead(int joi, JvnRemoteServer js) throws java.rmi.RemoteException, JvnException {
		Serializable obj = super.jvnLockRead(joi, js);

		// Save state
		this.saveState();

		return obj;
	}

	@Override
	public synchronized Serializable jvnLockWrite(int joi, JvnRemoteServer js) throws java.rmi.RemoteException, JvnException {
		Serializable obj = super.jvnLockWrite(joi, js);

		// Save state
		this.saveState();

		return obj;
	}

	@Override
	public synchronized void jvnTerminate(JvnRemoteServer js) throws java.rmi.RemoteException, JvnException {
		super.jvnTerminate(js);
		// Save state
		this.saveState();
	}

	public static void main(String[] args) {
		try {
			JvnCoordPersistentImpl jvnCoord = JvnCoordPersistentImpl.tryLoad("coord_backup.bak");
			System.out.println("Coordinator Ready");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

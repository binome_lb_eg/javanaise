package ircv2;

import jvn.annotations.JvnMethod;
import jvn.annotations.JvnMethod.AccessPolicy;

public interface SentenceI {

	@JvnMethod(AccessPolicy.WRITE)
	public void write(String text);

	@JvnMethod(AccessPolicy.READ)
	public String read();
}

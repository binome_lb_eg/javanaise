/***
 * Sentence class : used for keeping the text exchanged between users
 * during a chat application
 * Contact:
 *
 * Authors:
 */

package ircv2;

public class Sentence implements SentenceI, java.io.Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	String data;

	public Sentence() {
		data = new String("");
	}

	@Override
	public void write(String text) {
		data = text;
	}

	@Override
	public String read() {
		return data;
	}

}
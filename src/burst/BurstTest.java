package burst;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

import jvn.proxy.JvnObjectProxy;

/**
 * Stress test class
 */
public class BurstTest {
  /** Length of the test in seconds */
  static int TEST_TIME = 30;
  public static void main(String[] args) {
    if (args.length != 1) {
      System.out.println("Usage: burst <number_objects>");
      System.exit(1);
    }

    int nbObjects = Integer.parseInt(args[0]);

    // Create/get objects
    ArrayList<SampleObject> joList = new ArrayList<>();
    for (int i = 0; i < nbObjects; i++) {
      joList.add((SampleObject) JvnObjectProxy.newInstance(new SampleObjectImpl(), String.valueOf(i)));
    }

    AtomicBoolean shouldStop = new AtomicBoolean(false);
    // Set a timer to stop the test after a fixed amount of time
    Timer timer = new Timer();
    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        shouldStop.set(true);
      }
    }, TEST_TIME*1000); // Wait 30s before stopping

    long nbCalls = 0; // Number of methods called
    // Test loop
    while (shouldStop.get() == false) {
      // Lock Unlock every object
      for (SampleObject obj : joList) {
        obj.readMethod();
        obj.writeMethod(42);
      }
      nbCalls += joList.size() * 2;
    }

    // Print the number of method calls for benchmarking
    System.out.println(nbCalls + " method calls in " + TEST_TIME + "s (" + (nbCalls/TEST_TIME) + " calls per second)");
    System.exit(0);
  }
}

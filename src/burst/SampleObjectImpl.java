package burst;

public class SampleObjectImpl implements SampleObject {
	int a;

	@Override
	public int readMethod() {
		return a;
	}

	@Override
	public void writeMethod(int val) {
		this.a = val;
	}

}
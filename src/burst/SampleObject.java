package burst;

import java.io.Serializable;

import jvn.annotations.JvnMethod;
import jvn.annotations.JvnMethod.AccessPolicy;

/**
 * Sample interface for an object with both read and write methods
 */
public interface SampleObject extends Serializable {
	@JvnMethod(AccessPolicy.READ)
	int readMethod();

	@JvnMethod(AccessPolicy.WRITE)
	void writeMethod(int val);
}

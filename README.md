# Projet Javanaise

Binôme:
- Luc BERNARD
- Elian GUILLAUME

## Fonctionnalités implémentées

- Version 2 de IRC/Javanaise utilisant des **DynamicProxy** pour simplifier l'utilisation d'objets Javanaise.
- Persistence des données du Coordinateur après crash (`JvnCoordPersistentImpl`).
- Flush du cache côté Serveur.
- Stress test (Burst) avec mesures simples de performances.

## Exécution IRC v1/v2

Avec Eclipse, lancer un Coordinateur au choix (`JvnCoordImpl` ou `JvnCoordPersistentImpl`), puis lancer autant d'instances d'IRC (`v1` ou `v2`) que souhaité.

## Burst test

Le stress test Burst est composé d'un package Java (`src/burst`) et d'un script de lancement `./burstScript.sh`.

Pour exécuter le test, il suffit de lancer le script de lancement, qui se chargera de lancer un coordinateur et des processus Serveur qui effectueront des appels via l'interface Javanaise (pendant 30 secondes).


Le script prend en argument 2 valeurs:

1. Le nombre de processus Serveur à lancer
2. Le nombre d'objets en cache partagés


**Exemple :**

```sh
❯ ./burstScript.sh 4 4
## Burst Stress test for Javanaise
# Start Coordinator
Coordinator Ready
# Coordinator should be ready, start 4 processes with 4 objects
388831560 method calls in 30s (12961052 calls per second)
390404896 method calls in 30s (13013496 calls per second)
383512728 method calls in 30s (12783757 calls per second)
378737888 method calls in 30s (12624596 calls per second)
# Tests are done, killing Coordinator
```

## Bugs connus

- Quitter le coordinateur et les Serveurs IRCv2 puis les relancer ne marchera pas avant d'avoir supprimé le backup `coord_backup.bak`
→ Ceci est dû au fait que nous n'avons pas de gestion de panne côté Serveur, et le coordinateur ne fait donc pas le lien entre les anciens qu'il a sauvegardé et les nouvelles connexion.
